

public class Tester{
        public static void main(String[] args) throws InterruptedException {

        //テスト開始の出力処理
        //System.out.println("テストを開始します");

        //個人所有分の登録処理
        createUserMobileDevice("Taro","iPhoneX");
        createUserMobileDevice("Taro","Galaxy tablet");
        Notifier notifier = Notifier.getInstance();
        notifier.send("Taro","You habe 2 messages");
        Thread.sleep(500L);
        notifier.shutdown();

        //この後に追加コードが来るが上記でshutdown処理をしているので登録が無い状況
        //notifier.send("Taro","You have 2 message.");
        //Thread.sleep(500L);

    }


    private static void createUserMobileDevice(String user, String name){
        MobileDevice device = new MobileDevice(name, messageList ->
                System.out.println( name + ":" + messageList));

        Notifier notifier = Notifier.getInstance();
        notifier.register(user,device);
        new Thread(() -> {
            notifier.loopForMessages(device);
            System.out.printf("Terminate %s's' %s%n", user, name);
        }).start();
    }
}