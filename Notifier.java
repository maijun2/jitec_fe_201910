import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Notifier{
    private static final Notifier INSTANCE = new Notifier();

    private final Object lock = new Object();

    private final Map<String,List<MobileDevice>> userMobileDevices = new HashMap<>();
    private final Map<MobileDevice, List<String>> messagesToDeliver = new HashMap<>();

    private volatile boolean active = true;

    public static Notifier getInstance(){return INSTANCE;}

    private Notifier(){}

    public void register(final String user, final MobileDevice device) {
        synchronized (lock) {
            List<MobileDevice> devices = userMobileDevices.get(user);
            if (devices == null) {
                devices = new ArrayList<>();
                userMobileDevices.put(user, devices);
            }
            devices.add(device);
        }
    }

    public void send(final String user, final String message) {
        final List<MobileDevice> devices = new ArrayList<>();
        synchronized (lock) {
            if (userMobileDevices.containsKey(user)) {
                for (final MobileDevice device : userMobileDevices.get(user)) {
                    List<String> messageList = messagesToDeliver.get(device);
                    if (messageList == null) {
                        messageList = new ArrayList<>();
                        messagesToDeliver.put(device, messageList);
                    }
                    messageList.add(message);
                    devices.add(device);
                }
            }else{
                System.out.println("登録情報がありません。");
            }
        }
        for (final MobileDevice device : devices) {
            synchronized (device) {
                device.notifyAll();
            }
        }
    }

    public void loopForMessages(final MobileDevice device){
        while(active){
            List<String> messageList;
            synchronized(lock){
                messageList = messagesToDeliver.remove(device);
            }

            if(messageList != null){
                device.getListener().onNotificationReceived(messageList);
            }

            synchronized(device){
                try{
                    device.wait(3000L);
                } catch (final InterruptedException e){
                    break;
                }
            }
        }
    }

    public void shutdown(){
        active = false;
        List<MobileDevice> devices = new ArrayList<>();
        synchronized(lock){
            messagesToDeliver.clear();
            for(String user: userMobileDevices.keySet()){
                for(MobileDevice device : userMobileDevices.get(user)){
                    devices.add(device);
                }
            }
            userMobileDevices.clear();
        }
        for(MobileDevice device: devices){
            synchronized(device){
                device.notifyAll();
            }
        }

    }

}